#!/usr/bin/env python
# coding: utf-8

import os
import sys
import time
import getpass
import requests
import threading
from bs4 import BeautifulSoup

from selenium import webdriver

from selenium.webdriver.firefox.service import Service
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import invisibility_of_element_located

from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import ElementNotInteractableException


def format_url(url):
    """
    Formatage des noms de fichiers pour les avatars
    """
    return url.replace('//', '/').replace('/', '_').replace('_imgup.motion-twin.com_', '').replace('_data.twinoid.com_',"")


def blinking_dots(text, event):    
    """
    Texte de chargement clignotant
    """    
    while not event.is_set():
        for i in range(4):
            sys.stdout.write('\r' + text + '.' * i + ' ' * (3-i))
            sys.stdout.flush()
            time.sleep(0.5)
            if event.is_set():
                break
                

def run_with_loading_message(message, task_function, *args, **kwargs):
    """
    Exécute une tâche avec un message de chargement clignotant.
    """
    event = threading.Event()
    t = threading.Thread(target=blinking_dots, args=(message, event))
    t.start()
    task_function(*args, **kwargs)
    event.set()
    t.join()
    print()


def print_progress_bar(completed, total, start=0, discussion_id=None):    
    """
    Affichage d'une barre de progression basée sur le pourcentage de discussions complétées
    """    
    bar_length = 50
    completed = completed if start == 0 else completed + start - 1
    progress_length = int((completed / total) * bar_length)
    percentage = int((completed / total) * 100)
    progress_bar = '▌' + '█' * progress_length + '-' * (bar_length - progress_length) + '▐'
    print(f"\r> {discussion_id} {progress_bar} {percentage}% ({completed}/{total}) <", end="")

    
def download_images(urls, folder):    
    """
    Téléchargement des images si elles n'existent pas déjà seulement
    """    
    for url in urls:
        filename = format_url(url) if folder == 'avatars' else url.split('/')[-1]
        save_path = f'./img/{folder}/{filename}'
        if not os.path.exists(save_path):
            response = requests.get(url if url.startswith('http') else 'http:' + url, stream=True)
            with open(save_path, 'wb') as f:
                for chunk in response.iter_content(8192):
                    f.write(chunk)    
    

def insert_discuss_threads_iframe(soup, data_name):    
    """
    Insertion de la liste des discussions à gauche de la discussion actuelle
    """
    if data_name == "discussions":
        iframe = soup.new_tag("iframe", src="../index.html", style="width:20%; height:100vh; border: none; flex-shrink: 0; overflow-y: scroll;")
    elif data_name == "archives":
        iframe = soup.new_tag("iframe", src="../indexArchives.html", style="width:20%; height:100vh; border: none; flex-shrink: 0; overflow-y: scroll;")

    main_container = soup.find("div", id="frameContent")
    flex_container = soup.new_tag("div", style="display: flex; width: 100%; height: 100vh; overflow-y: hidden;")

    for child in list(main_container.children):
        child.attrs["style"] = "overflow-y: scroll; height: 100vh; width: 80%;"
        flex_container.append(child.extract())
        
    flex_container.insert(0, iframe)
    main_container.append(flex_container)


def auto_scroll_discussions(driver):
    """
    Auto-scrolling de la liste des discussions
    """
    while True:
        buttons = driver.find_elements(By.CSS_SELECTOR, "tr.buttonAutoScroll")
        if not buttons:
            break
        tid_start_before = buttons[0].get_attribute("tid_start")
        buttons[0].click()
        try:
            WebDriverWait(driver, 20).until(
                lambda x: driver.find_element(By.CSS_SELECTOR, "tr.buttonAutoScroll").get_attribute("tid_start") != tid_start_before
            )
        except TimeoutException:
            break
            
            
def auto_scroll_messages(driver):
    """
    Auto-scroll le contenu d'une discussion.
    """
    while True:
        start_idx_element = WebDriverWait(driver, 20).until(
            EC.presence_of_element_located((By.ID, "discussStartIdx"))
        )
        start_idx = start_idx_element.get_attribute("value")
        if start_idx == "0":
            break

        driver.execute_script("document.getElementById('scroll').style.overflowX = 'hidden';")
        buttons = driver.find_elements(By.CSS_SELECTOR, "a.buttonAutoScroll")
        if not buttons:
            break
            
        buttons[0].click()
        try:
            WebDriverWait(driver, 20).until(
                lambda x: driver.find_element(By.ID, "discussStartIdx").get_attribute("value") != start_idx
            )
        except TimeoutException:
            break


def initialize_browser():
    """
    Initialisation du navigateur
    """
    options = Options()
    options.add_argument('--headless')
    service = Service('./geckodriver.exe')
    driver = webdriver.Firefox(service=service, options=options)
    return driver


def login_to_twinoid(driver):
    """
    Connexion Twinoid
    """
    # 1- Récupération des identifiants TwinoiD de l'utilisateur
    # 2- Parcours des iframes pour trouver le formulaire de connexion
    # 3- Accès et connexion à la plateforme TwinoiD
    
    driver.get('https://twinoid.com')

    print("\n\n\n")
    print("=============================================================")
    print("========================  CONNEXION  ========================")
    print("=============================================================")
    print("\n> Veuillez entrer vos identifiants Twinoid :\n")
    
    twinoid_email = input("Email : ")
    twinoid_password = getpass.getpass("Mot de passe : ")
    
    print("\n")
    login_button = driver.find_element(By.CSS_SELECTOR, "a.tid_login")
    login_button.click()
    wait = WebDriverWait(driver, 20)
    iframes = driver.find_elements(By.TAG_NAME, "iframe")

    for iframe in iframes:
        driver.switch_to.frame(iframe)
        try:
            email_field = wait.until(EC.element_to_be_clickable((By.NAME, "login")))
            if email_field:
                break
        except:
            driver.switch_to.default_content()

    password_field = wait.until(EC.element_to_be_clickable((By.NAME, "pass")))
    email_field.send_keys(twinoid_email)
    password_field.send_keys(twinoid_password)
    login_submit_button = driver.find_element(By.ID, "loginButton")
    login_submit_button.click()


def download_avatars_and_icons(html_content):
    """
    Téléchargement des images (icônes et avatars)
    """
    # 1- Téléchargement des avatars (focus sur le bloc de participants)
    # 2- Téléchargement des icônes
    
    soup = BeautifulSoup(html_content, 'html.parser')
    
    thread_peers = soup.find('ul', id='threadPeers')
    if not thread_peers:
        return
    
    avatar_urls = [img['src'] for img in thread_peers.select('.tid_ula')]
    download_images(avatar_urls, 'avatars')    
    
    icon_urls = [img['src'] for img in soup.find_all('img', class_='tid_ico')]
    download_images(icon_urls, 'src')


def clean_html(soup, filename, data_name):  
    """
    Cleaning du fichier HTML
    """
    # 1- Suppression du masquage des joueurs dans les groupes de discussions
    # 2- Suppression des éléments indésirables dans le fichier
    # 3- Suppresion de certaines balises, tout en conservant leur contenu
    # 4- Dans le fichier de discussion :
    # >>> a- Gestion des avatars plus intelligente et peu volumineuse
    # >>> b- Suppressions d'éléments indésirables (attributs tid_*)
    # >>> c- Ajout d'un lien de redirection vers une discussion quand on clique dessus

    selectors_to_remove = [
        ("div", "class", "messageMenu"),
        ("form", "id", "post"),
        ("div", "class", "crel"),
        ("div", "id", "bottomShadow"),
        ("div", "id", "topShadow"),
        ("img", "class", "cursor"),
        ("div", "class", "menu")
    ]        
    
    for li_tag in soup.find_all("li", class_="peer hidden"):
        li_tag["class"] = ["peer"]

    for tag, attr, value in selectors_to_remove:
        for element in soup.find_all(tag, **{attr: value}):
            element.decompose()

    for li in soup.find_all("li", class_="label"):
        if li.find("a", onclick=True):
            li.decompose()

    for element in soup.find_all(id="scroll"):
        element.unwrap()
        
    for img_tag in soup.find_all('img', src=True):
        old_src = img_tag['src']
        if old_src.startswith("//imgup.motion-twin.com") or old_src == "//data.twinoid.com/img/moderatorAvatar.png":
            new_src = format_url(old_src)
            img_tag['src'] = f"./img/avatars/{new_src}" if "index" in filename else f"../img/avatars/{new_src}"

        
    for img_tag in soup.find_all('img', class_='tid_ico'):
        old_src = img_tag['src']
        new_src = old_src.split('/')[-1]
        img_tag['src'] = f"./img/src/{new_src}" if "index" in filename else f"../img/src/{new_src}"
        
        
    if "index" in filename:

        threads_wrapper = soup.select_one('.threadsWrapper')
        if threads_wrapper and threads_wrapper.has_attr('style'):
            threads_wrapper['style'] = "height: auto;"
    
        for element in soup.find_all(True):
            attrs_to_delete = [attr for attr in element.attrs if attr.startswith("tid_")]
            for attr in attrs_to_delete:
                del element.attrs[attr]
        
            if 'onclick' in element.attrs:
                del element.attrs['onclick']
            
        for tr_tag in soup.find_all("tr", id=lambda x: x and x.startswith("discussThread_")):
            discussion_id = tr_tag['id'].replace("discussThread_", "")
            a_tag = tr_tag.find("a")
            if a_tag:
                a_tag['href'] = f"discuss/discussThread_{discussion_id}.html"
                a_tag['target'] = '_top'

    if "Archives" in filename:
        soup.find("td", class_="threadsPanel")["class"].append("active")
            
    if "discussThread_" in filename:
        insert_discuss_threads_iframe(soup, data_name)


def save_to_html(filename, content, data_name=None):  
    """
    Sauvegarde du contenu dans un fichier HTML
    """
    # 1- Initialisation du squelette HTML initial
    # 2- Lecture du fichier et application des modifications
    # 3- Mise en forme du fichier et sauvegarde
    
    directory = "./discuss" if filename not in ["index.html", "indexArchives.html"] else "./"
    filename = os.path.join(directory, filename)
    
    with open(filename, 'w', encoding='utf-8') as file:
        file.write("<html><head>")
        
        if "index" in filename:
            file.write('<link rel="stylesheet" type="text/css" href="./css/bar.css">')
            file.write('<link rel="stylesheet" type="text/css" href="./css/style.css">')
        else:
            file.write('<link rel="stylesheet" type="text/css" href="../css/bar.css">')
            file.write('<link rel="stylesheet" type="text/css" href="../css/style.css">')
            
        file.write("</head><body id='frame' style='background:transparent' class='tid_ff tid_v109 fullFrame topWindow tid_lang_fr'>")
        
        if "discussThread_" in filename:
            file.write('<h1 style="background-color: #3b4151; color: white; padding: 10px; font-size: 24px; width: 100%;">')
            file.write('    Messagerie privée Twinoid\n')
            file.write('</h1>\n')
            
        file.write("<div id='frameCenter'>")
        file.write("<div id='frameContent' class='discuss'>")
        file.write("<div id='frameBg'>")
        file.write("<table class='layout full'><tr>")
        file.write(content)
        file.write("</tr></table></div></div></div></body></html>")
    
    with open(filename, 'r', encoding='utf-8') as file:
        soup = BeautifulSoup(file, 'html.parser')
    
    clean_html(soup, filename, data_name)    
    
    with open(filename, 'w', encoding='utf-8') as file:
        file.write(soup.prettify(formatter="html"))   


def analysis_type(data, data_name):
    """
    Type de sauvegarde (complète, reprise, màj) pour la sauvegarde des discussions
    """
    print(f"\n> Type de sauvegarde :\n")
    print("(0) Skip")
    print(f"(1) Complète : Sauvegarde de toutes les {data_name}")
    print(f"(2) Reprise  : Sauvegarde des {data_name} à partir de la Xème")
    print(f"(3) MàJ      : Sauvegarde des x {data_name} les plus récentes")
    
    max_discussion = len(data)
    
    while True:
        choice = input("\nVeuillez choisir le type de sauvegarde (0/1/2/3) : ")
        if choice == "0":
            print("=> Choix retenu : Skip")
            return 0, 0
        elif choice == "1":
            print("=> Choix retenu : Sauvegarde complète")
            return 0, max_discussion
        elif choice == "2":
            while True:
                try:
                    user_input = input(f"(2) Entrez le numéro de la {data_name} de départ (entre 1 et {max_discussion}) : ")
                    if 1 <= int(user_input) < max_discussion:
                        print(f"=> Choix retenu : Sauvegarde de reprise (à partir de la {int(user_input)}ème {data_name})")
                        return int(user_input), max_discussion
                    else:
                        print(f"Le numéro doit être compris entre 1 et {max_discussion}. Veuillez réessayer.")
                except ValueError:
                    print("Veuillez entrer un nombre valide.")
            break
        elif choice == "3":
            while True:
                try:
                    user_input = input(f"(3) Entrez le nombre de {data_name} les plus récentes à sauvegarder (entre 1 et {max_discussion}) : ")
                    if 1 <= int(user_input) <= max_discussion:
                        print(f"=> Choix retenu : Sauvegarde de mise à jour (les {int(user_input)} {data_name} les plus récentes)")
                        return 0, int(user_input)
                    else:
                        print(f"Le nombre doit être compris entre 1 et {max_discussion}. Veuillez réessayer.")
                except ValueError:
                    print("Veuillez entrer un nombre valide.")
            break
        else:
            print("Choix invalide. Veuillez choisir 0, 1, 2 ou 3.")
            

def analyze_and_save(driver, data_name):
    """
    Analyse et sauvegarde des discussions dans l'onglet sélectionné
    """
    discussions = driver.find_elements(By.CSS_SELECTOR, "tr[id^='discussThread_']")
    
    start, end = analysis_type(discussions, data_name)
    if start == 0 and end == 0:
        return
    
    print("\n\n> Sauvegarde en cours")
    slice_start = start-1 if start > 0 else start
    slice_end = end
    for idx, discuss in enumerate(discussions[slice_start:slice_end], start=1):
        discussion_id = discuss.get_attribute("id").split("_")[1]
        discuss.click()
        print_progress_bar(idx, end, start, discussion_id)
        download_avatars_and_icons(driver.find_element(By.ID, "discussThread").get_attribute("outerHTML"))
        discussLastIdx = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'discussLastIdx')))
        if int(discussLastIdx.get_attribute('value')) == 4000:
            driver.execute_script(f"document.getElementById('post').innerHTML += '<input type=\"hidden\" id=\"discussThreadId\" value=\"{discussion_id}\">'")
        auto_scroll_messages(driver)
        save_to_html(f"discussThread_{discussion_id}.html", driver.find_element(By.ID, "discussThread").get_attribute("outerHTML"), data_name)
    
        
def analysis(driver):
    """
    Boucle sur chaque discussion pour sauvegarder leur contenu + listing des discussions
    """   
    # 1- Ouverture des MPs
    # 2- Sauvegarde de l'avatar utilisateur
    # 3- Auto-scroll pour charger toutes les discussions
    # 4- Sauvegarde des discussions dans index.html
    # 5- Sauvegarde itérative du contenu de chaque discussion (auto-scroll pour charger leur contenu)
    
    # Ouverture des MPs 
    driver.get('https://twinoid.com/discuss')
    WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR, "tr[id^='discussThread_']")))
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    
    # Sauvegarde de l'avatar utilisateur + modération
    twino_avatar_url = soup.select_one('.tid_twinoidAvatar .tid_avatarImg')['src']
    with open('./img/avatars/{}'.format(format_url(twino_avatar_url)), 'wb') as file:
        file.write(requests.get('http:' + twino_avatar_url).content)
    
    admin_avatar_url = '//data.twinoid.com/img/moderatorAvatar.png'
    with open('./img/avatars/{}'.format(format_url(admin_avatar_url)), 'wb') as file:
        file.write(requests.get('http:' + admin_avatar_url).content)

    # Listing et sauvegarde des discussions
    print("\n")
    print("=============================================================")
    print("======================== DISCUSSIONS ========================")
    print("=============================================================")
    print("\n")
    run_with_loading_message("> Listing des discussions en cours", auto_scroll_discussions, driver)
    run_with_loading_message("> Sauvegarde de l'index des discussions en cours", save_to_html, "index.html", driver.find_element(By.ID, "discussThreads").get_attribute("outerHTML"))
    analyze_and_save(driver, "discussions")
    
    # Listing et sauvegarde des archives
    print("\n\n\n")
    print("=============================================================")
    print("========================   ARCHIVES   =======================")
    print("=============================================================")
    driver.find_element(By.CLASS_NAME, "tabArchive").click()
    WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR, "tr[id^='discussThread_']")))
    run_with_loading_message("\n\n> Listing des archives en cours", auto_scroll_discussions, driver)
    run_with_loading_message("> Sauvegarde de l'index des archives en cours", save_to_html, "indexArchives.html", driver.find_element(By.ID, "discussThreads").get_attribute("outerHTML"))
    analyze_and_save(driver, "archives")
    
    print("\n\n\n")
    print("=============================================================")
    print("=============================================================")
    print("                    SAUVEGARDE TERMINÉE !                    ")
    print("=============================================================")
    print("=============================================================")


def main():
    driver = initialize_browser()
    login_to_twinoid(driver)
    analysis(driver)


if __name__ == "__main__":
    main()