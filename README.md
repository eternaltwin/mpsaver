# MPSaver

## Prérequis

- Télécharger le contenu de ce repo
- Télécharger Python depuis le Microsoft Store
- Vérifier que FireFox est à jour (paramètre > aide > à propos de firefox) ou téléchargez le [directement](https://www.mozilla.org/fr/firefox/new/).
- Télécharger [Geckodriver](https://github.com/mozilla/geckodriver/releases) et le placer au même endroit que le script (MPSaver.py).
- Ouvrir un invite de commande (cmd dans la barre de recherche)
- Taper cette ligne de code :

```sh
pip install requests beautifulsoup4 selenium
```

- Aller dans le répertoire ayant le script MPSaver.py

```sh
cd C:\Users\Eliam\Desktop\MPSaver
```

- Lancer le script

```sh
python MPSaver.py
```

- **Connexion Twinoid** : Renseigner votre **mail** / **mdp**.
- **Discussion** : Choisir le type de sauvegarde (complète, reprise, mise à jour) et attendre la fin de la sauvegarde.
- **Archives** : Choisir le type de sauvegarde (complète, reprise, mise à jour) et attendre la fin de la sauvegarde.
- Une fois le script terminé, il vous sera possible de consulter vos conversations en ouvrant index.html et les archives en ouvrant indexArchives.html.

## Remerciements

Merci à Eliam pour ce merveilleux script.
